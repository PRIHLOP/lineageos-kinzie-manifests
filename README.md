<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [LineageOS with microG for kinzie in docker build machine](#lineageos-with-microg-for-kinzie-in-docker-build-machine)
    - [Initial setup](#initial-setup)
        - [Docker image changes](#docker-image-changes)
        - [Docker image build](#docker-image-build)
        - [Create working directories](#create-working-directories)
        - [Place device information in local_manifests](#place-device-information-in-local_manifests)
- [LineageOS build](#lineageos-build)
    - [Run the docker image](#run-the-docker-image)
    - [Access the build environment directly if needed](#access-the-build-environment-directly-if-needed)
        - [repo sync failing?](#repo-sync-failing)
    - [Build for kinzie with the Lineage for microG additions, and with TWRP](#build-for-kinzie-with-the-lineage-for-microg-additions-and-with-twrp)
- [TWRP](#twrp)
    - [Encryption](#encryption)
- [kinzie specific information](#kinzie-specific-information)
    - [Building the motorola msm kernel](#building-the-motorola-msm-kernel)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# LineageOS with microG for kinzie in docker build machine

device rom currently untested

[LineageOS for microG][lineagemicrog]

## Initial setup

Build the [lineageos4microg docker image][lineageos4microg_repo]
```bash
git clone https://github.com/lineageos4microg/docker-lineage-cicd.git
cd docker-lineage-cicd
```

### Docker image changes

Poor internet?:  
In `src/build.sh`, on repo sync commands, add `--jobs=1`.

### Docker image build

```bash
docker build -t lineage .
```

### Create working directories

Adjust these paths to suit.
```bash
mkdir /mnt/android_build/lineageos
mkdir -p /mnt/android_build/lineageos/{ccache,source,zips,local_manifests,lineage_keys,logs,lineage_scripts}
```

### Place device information in local_manifests

- Copy into the folder that is passed into the docker image as `/srv/local_manifests`:
  - 0_muppets.xml
  - kinzie_lineage.xml
  - lineageos4microg_prebuilts.xml
  - twrp.xml
- Copy into the folder passed as `/srv/userscripts`:
  - before.sh

Explanation:
- 0_muppets.xml
  - [TheMuppets][themuppets_repo] proprietary files extracted from stock roms, for offical LineageOS device builds
  - Vendors do not supply all source code, so we must copy some of their prebuilt files to get a working device
  - This is not currently used for kinzie
  - This can be left out, but requires changes in kinzie_lineage.xml
- kinzie_lineage.xml
  - Tells the build system what is required to build LineageOS for kinzie, and where the source filese are
  - This is required for kinzie
- lineageos4microg_prebuilts.xml
  - Tells the build system to include MicroG, F-Droid, and a few other (F-Droid built) applications in the rom, see [lineagemicrog] for details
  - Requires `"CUSTOM_PACKAGES=GmsCore GsfProxy FakeStore FDroid FDroidPrivilegedExtension MozillaNlpBackend NominatimNlpBackend com.google.android.maps.jar"`
  - This can be left out is these applications are not wanted
- twrp.xml
  - Tells the build system what is required to build TWRP, and where the source files are ([omnirom/android_bootable_recovery][omnirom_recovery_repo])
  - The kinzie device tree requires the environmental variable `WITH_TWRP=TRUE` to build TWRP
  - This can be left out if TWRP is not wanted, but don’t use `WITH_TWRP=TRUE`
- before.sh
  - [Patch to fix TWRP build with LineageOS][omnirom_gerrit_22096]

Differences between [github.com/lineageos-for-kinzie][kinzie_repo] and [gitlab.com/android-lineageos-kinzie-johnth/lineageos-for-kinzie][gitlab-kinzie]. [device differences][github_diff_device] [vendor differences][github_diff_vendor]
- vendor blobs fixed to reference qcom blobs in repo
 
# LineageOS build

## Run the docker image

The initial full sync is around 30GB download. This only needs to be fetched once.
```bash
docker run \
  --name "lineage_builder" \
  --rm \
  --detach \
  --tty \
  --volume="/mnt/android_build/lineageos/source:/srv/src" \
  --volume="/mnt/android_build/lineageos/zips:/srv/zips" \
  --volume="/mnt/android_build/lineageos/lineage_scripts:/srv/userscripts" \
  --env="BRANCH_NAME=cm-14.1" \
  --env="CRONTAB_TIME=now" \
  --env="DEVICE_LIST=kinzie" \
  lineage \
)
```

Follow progress by attaching to the docker container: `docker attach lineage_builder`  
Detach with `Ctrl+c`. This is not an `--interactive` tty, so this is not passed through to the container.

## Access the build environment directly if needed

The Docker image is setup to do everything. If wanted for debug, the build envorinment can be accessed directly using:
```bash
cp -a /mnt/android_build/lineageos/local_manifests/*.xml /mnt/android_build/lineageos/source/.repo/local_manifests/

docker run \
  --name=lineage-$(date +%Y%m%d_%H%M) \
  --rm \
  --interactive \
  --tty \
  --volume="/mnt/android_build/lineageos/source:/srv/src" \
  --entrypoint=/bin/bash \
  --workdir="/srv/src" \
  lineage
```
column not found: `apt-get update; apt-get install bsdmainutils`

### repo sync failing?

Use `repo sync --force-broken --force-sync --prune --jobs=1` in the build environment (previous command to get there).

Manually run repo sync, with
- forcing to continue on errors
- only use one connection

repo source code is found here for usage information: [git-repo]

## Build for kinzie with the Lineage for microG additions, and with TWRP
```bash
docker run \
  --name=lineage-$(date +%Y%m%d_%H%M) \
  --rm \
  --detach \
  --tty \
  --env="CRONTAB_TIME=now" \
  --env="CCACHE_SIZE=200G" \
  --env="USER_NAME=John T" \
  --env="USER_MAIL=lineage@example.com" \
  --env="WITH_SU=false" \
  --env="BRANCH_NAME=cm-14.1" \
  --env="DEVICE_LIST=kinzie" \
  --env="OTA_URL=" \
  --env="SIGN_BUILDS=false" \
  --env="CLEAN_OUTDIR=false" \
  --env="CLEAN_AFTER_BUILD=false" \
  --env="ZIP_SUBDIR=true" \
  --env="LOGS_SUBDIR=true" \
  --env="DELETE_OLD_ZIPS=3" \
  --env="DELETE_OLD_LOGS=3" \
  --env="RELEASE_TYPE=microG" \
  --env="SIGNATURE_SPOOFING=restricted" \
  --env="CUSTOM_PACKAGES=GmsCore GsfProxy FakeStore FDroid FDroidPrivilegedExtension MozillaNlpBackend NominatimNlpBackend com.google.android.maps.jar" \
  --env="WITH_TWRP=true" \
  --volume="/mnt/android_build/lineageos/source:/srv/src" \
  --volume="/mnt/android_build/lineageos/local_manifests:/srv/local_manifests" \
  --volume="/mnt/android_build/lineageos/zips:/srv/zips" \
  --volume="/mnt/android_build/lineageos/logs:/srv/logs" \
  --volume="/mnt/android_build/lineageos/lineage_scripts:/srv/userscripts" \
  --volume="/mnt/android_build/lineageos/lineage_keys:/srv/keys" \
  --volume="/mnt/android_build/lineageos/ccache:/srv/ccache" \
  lineage
```

# TWRP

TWRP needs a patch to successfully build with LineageOS 14, arm64 and Qualcom libcryptfs. The patch application can be automated in the docker lineage build machine using /srv/userscripts/before.sh, with:

```bash
repopick -P /srv/src/bootable/recovery-twrp -g https://gerrit.omnirom.org 22096
```

This is included in the `before.sh` file in this repo.

Use `WITH_TWRP=true` in the build command to get the docker lineageos build machine to build TWRP.

Sources:
- [xda TWRP compile libcryptfs_hw.so issue][xda_twrp_libcryptfs]
- [TWRP cherry pick commit to fix libcryptfs_hw.so issue][omnirom_gerrit_22096]
- [xda Galaxy S5 TWRP][xda_twrp_example]
- [xda-developers kinzie TWRP (Xadro)][xda_kinzie_twrp]

## Encryption

There is no support for encryption in TWRP on kinzie.
It needs:
 - A change to TWRP fstab, to show that the encryption header is on another paritions
 - The vendor files to access the encryption hardware

See:
- [TWRP Clark device tree][twrp_clark]
- [TWRP Clark add encryption support 1][twrp_clark_crypto1]
- [TWRP Clark add encryption support 2][twrp_clark_crypto2]

# kinzie specific information

Sources:
- [xda-developers LineageOS 14.1 for Kinzie (jwchen119)][xda_kinzie_lineage]
- [xda Xadro kinzie GitHub repo][github_kinzie_l13]
- Motorola:
  - [kernel-msm][moto_kernel-msm]
  - [motorola-kernel][moto_kernel]
  - [motorola build instructions for X Style][moto_instructions]

Siblings:
[Snapdragon 808/810 phones][wiki_snap810]
- [Moto X Style (clark)][wiki_clark] [Lineage device tree][lineage_clark]
- [Huawei Nexus 6P (angler)][wiki_angler] [Lineage device tree][lineage_angler]
- [OnePlus2 (oneplus2)][wiki_op2] [Lineage device tree][lineage_op2]

## Building the motorola msm kernel

- Need in manifest:
  - MotorolaMobilityLLC/kernel-msm
  - MotorolaMobilityLLC/motorola-kernel
  - MotorolaMobilityLLC/vendor-qcom-opensource-wlan-qcacld-2.0"
- In device/motorola/kinzie:
  - Change `TARGET_KERNEL_SOURCE`
  - Change `TARGET_KERNEL_CONFIG`
- In MotorolaMobilityLLC/kernel-msm
  - Make changes to drivers/staging Kconfig and Makefile to include qcacld-2.0. See [clark_add_qcacld-2.0].
  - Link qcacld-2.0: `cd drivers/staging && ln -sf ../../../kernel_drivers_staging_qcacld-2.0 qcacld-2.0`

# Snippets

Clean recovery
```bash
sudo rm -r source/{{device,vendor}/motorola/kinzie,out/target/product/kinzie/recovery*}
```

Search for strings in makefile
```bash
grep -R 'TARGET_KEYMASTER_WAIT_FOR_QSEE' $(find source -iname '*.mk' 2>/dev/null)
```

Search for libs in ELF
```bash
strings qseecomd | grep ^lib
```

Read stock full xml zip system image [source][moto_s2img]
```bash
simg2img tmp/system.img_sparsechunk.* tmp/system.img.tmp
offset=`LANG=C grep -aobP -m1 '\x53\xEF' tmp/system.img.tmp | head -1 | awk '{print $1 - 1080}'`
dd if=tmp/system.img.tmp of=system.img ibs=$offset skip=1 >& tmp/dd.log
```

[//]: # (Sources)

[twrp_clark]: https://github.com/TeamWin/android_device_moto_clark/tree/cm-13.0

[twrp_clark_crypto1]: https://github.com/TeamWin/android_device_moto_clark/commit/713711e11e1582c66dc4a4cd5a7bdecdf220fa74

[twrp_clark_crypto2]: https://github.com/TeamWin/android_device_moto_clark/commit/5deb0de3c4323359878ed108f12cab90d1e404c8

[xda_kinzie_lineage]: https://forum.xda-developers.com/droid-turbo-2/development/rom-cyanogenmod-14-1-kinzie-t3536148

[kinzie_repo]: https://github.com/lineageos-for-kinzie

[xda_kinzie_twrp]: https://forum.xda-developers.com/droid-turbo-2/development/recovery-t3306253

[github_kinzie_l13]: https://github.com/cm-kinzie-porting

[lineagemicrog]: https://lineage.microg.org

[lineageos4microg_repo]: https://github.com/lineageos4microg/docker-lineage-cicd/tree/master

[omnirom_recovery_repo]: https://github.com/omnirom/android_bootable_recovery

[themuppets_repo]: https://github.com/TheMuppets/manifests

[xda_twrp_libcryptfs]: https://forum.xda-developers.com/xperia-z4/development/recovery-twrp-v3-0-3-0-encryption-t3564016/page2

[omnirom_gerrit_22096]:https://gerrit.omnirom.org/#/c/22096/

[xda_twrp_example]: https://forum.xda-developers.com/galaxy-s5/help/compiling-twrp-t3554761

[moto_kernel-msm]: https://github.com/MotorolaMobilityLLC/kernel-msm/releases/tag/MMI-NPK25.200-12

[moto_kernel]:https://github.com/MotorolaMobilityLLC/motorola-kernel/releases/tag/MMI-NPK25.200-12

[moto_instructions]: https://github.com/MotorolaMobilityLLC/readme/blob/83f7af77234a919b06be54087358cd0b4eaf3477/MMI-MPH24.49-20.txt

[clark_add_qcacld-2.0]: https://github.com/LineageOS/android_kernel_motorola_msm8992/commit/1e09f0a79350db14822ed2b04f72cf2c890099b6

[git-repo]: https://gerrit.googlesource.com/git-repo/

[gitlab-kinzie]: gitlab.com/android-lineageos-kinzie-johnth/lineageos-for-kinzie

[github_diff_device]: https://github.com/lineageos-for-kinzie/android_device_motorola_kinzie/compare/master...john-tho:working

[github_diff_vendor]: https://github.com/lineageos-for-kinzie/android_vendor_motorola_kinzie/compare/master...john-tho:working

[wiki_snap810]: https://en.wikipedia.org/wiki/List_of_Qualcomm_Snapdragon_systems-on-chip#Snapdragon_808_and_810

[wiki_clark]: https://en.wikipedia.org/wiki/Moto_X_Style

[lineage_clark]: https://github.com/LineageOS/android_device_motorola_clark

[wiki_angler]: https://en.wikipedia.org/wiki/Nexus_6P

[lineage_angler]: https://github.com/LineageOS/android_device_huawei_angler

[wiki_op2]: https://en.wikipedia.org/wiki/OnePlus_2

[lineage_op2]: https://github.com/LineageOS/android_device_oneplus_oneplus2

[moto_s2img]: https://github.com/luk1337/Moto_sparse2img/blob/master/convert.sh
