#!/bin/bash

source build/envsetup.sh

# TWRP fix for Qualcom libcryptfs

which repopick
repopick -P /srv/src/bootable/recovery-twrp -g https://gerrit.omnirom.org 22096
